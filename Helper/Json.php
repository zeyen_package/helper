<?php

/**
 * PHP Json处理类
 * @author Zeyen <zeyen0186@aliyun.com>
 * @date 2021-05-24
 * Class Json
 */
namespace Helper;
class Json {



    public static function encode($value, $options = 320): string

    {

        $expressions =array();

        $value =  self::processData($value);

        $json = urldecode(json_encode($value));



        return $json;

    }

    public static function decode($json, $asArray = true)

    {
        if (is_array($json)) {

            throw new Exception('Invalid JSON data');

        }

        $decode = json_decode((string) $json, $asArray);

        return $decode;

    }

    protected static function processData($data)

    {

        if (is_array($data)){

            foreach ($data as $key=>$val){

                $data[$key] = self::processData($val);

            }

        }elseif (is_object($data)){

            $vars = get_object_vars($data);

            foreach ($vars as $key=>$val){

                $data->{$key} = self::processData($val);

            }

        } else {

            $data = urlencode($data);

        }

        return $data;

    }

}