<?php


namespace Helper;


/**
 * 图片处理助手类
 * @author Zeyen <zeyen0186@aliyun.com>
 * @date 2021-05-24
 * Class Image
 * @package Helper
 */
class Image
{
    /**
     * 图片base64字符串转文件保存
     * @param $base64_image_content  //图片base64字符串内容
     * @param string $save_path      //保存的路劲
     * @return false|string
     */
    public static function base64ImageSave($base64_image_content,$save_path="uploads/")
    {
        //匹配出图片的格式
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)) {
            $type = $result[2];
            $new_file_path = $save_path . date('Ymd', time()) . "/";
            if (!file_exists($new_file_path)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($new_file_path, 0700);
            }
            $new_file = $new_file_path . time().uniqid(). ".{$type}";
            if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))) {
                return '/' . $new_file;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 将保存的图片路径字符串以一个标记分割数组，并且拼接域名
     * @param $image_string  //字符串
     * @param string $stamp  //分割标记
     * @param null $domain   //拼接的域名
     * @return array
     */
    public function imageString2Array($image_string,$stamp=',',$domain=null): array
    {
        $image_array =explode($stamp,$image_string);
        if($image_array){
            if($domain){
                $array=[];
                foreach ($image_array as $key=>$value){
                    $array[$key]=$domain.$value;
                }
                return  $array;
            }else{
                return  $image_array;
            }
        }else{
            return [];
        }
    }
}