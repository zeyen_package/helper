# helper

#### 介绍
Zeyen个人的一些常用开发方法函数包

#### 软件架构
基于Zeyen个人平时开发中经常使用的一些函数和方法进行重新构建封装，方便开发，后续会持续更新，敬请期待~


#### 安装教程

1.  检测电脑中是否安装composer，如若没有，请参考composer教程安装composer
2.  在项目更目录执行 composer require zeyen/helper，进行安装,或者添加在项目的composer.json文件中，再执行composer update命令

#### 类方法说明

1. 客户端助手类 ——class Client{ }

   1. 获取用户操作系统——getOs( ){ };

   2. 获取用户浏览器类型和版本——getBrowser( ){ };

   3. 获取用户客户端IP——getClientIp( ){ };

   4. 获取设备类型（安卓 or 苹果）——getDeviceType( ){ };

   5. 判断是否为微信端——isWechat( ){ };

      

2. 数据处理助手类——class DataHelper{ }

   1. 常用的Api接口返回json数据格式输出——jsonOutput( ){ };

   2. 对象数据转数组数据——object2array( ){ };

   3. 将数组组装成XML格式——data2Xml( ){ };

   4. 将Xml格式转为Array——xml2Data( ){ };

   5. 仿ThinkPhP dump()函数，使得打印输出数据更加规范好看 ——dump( ){ };

      

3. 图片处理助手类——class Image{ }

   1. 图片base64字符串转文件保存——base64ImageSave( ){ };
   2. 将保存的图片路径字符串以一个标记分割数组，并且拼接域名——imageString2Array( ){ };

      

4.  Json处理类——class Json{ }

    1.  编码encode——encode( ){ };

    2.  解码decode——decode( ){ };

        

5.  网络请求助手类——class Request{ }

    1.  CURl HTTP请求（支持HTTP/HTTPS，支持GET/POST）——httpRequest( ){ };

    2.  POST提交XML文件，适用于微信系列的CURL请求——postXmlCurl( ){ };

        

6.  字符串处理助手类——class StringHelper{ }

    1.  字符串可逆加解密方法——encrypt( ){ };

7.  未完待续~~~

    

#### 作者信息

1.  Zeyen  邮箱 zeyen0186@aliyun.com
2.  Zeyen 官方博客 [www.zeyen.cn](https://www.zeyen.cn)
3.  你可以 [https://gitee.com/athenazetan](https://gitee.com/athenazetan) 这个地址来了解 Zeyen的其他优秀开源项目
